import Vue from 'vue'
import VueRouter from 'vue-router'
import PostList from '../components/PostList';
import Post from '../components/Post';
import NotFound from '../components/NotFound';

Vue.use(VueRouter)

const routes =  [
    { path: '/', component: PostList },
    { path: '/post/:id', component: Post },
    { path: '*', component: NotFound }
];

export default new VueRouter({
    mode: 'history',
    routes, // short for `routes: routes`
})
  