import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { jsonapiModule } from 'jsonapi-vuex'
import createLogger from 'vuex/dist/logger'

Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'

const api = axios.create({
    baseURL: 'http://api.lvh.me/api/v1/',
    headers: {
        'Content-Type': 'application/vnd.api+json',
    },
})

export default new Vuex.Store({
  strict: debug,
  plugins: debug ? [createLogger()] : [],
  modules: {
    jv: jsonapiModule(api),
  },
})
  