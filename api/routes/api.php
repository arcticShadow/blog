<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function () {
    JsonApi::register('v1')
    ->withNamespace('Api')
    ->routes(function ($api) {
        $api->resource('posts')->relationships(function ($relations) {
            $relations->hasOne('user')->except('replace');
        });

        $api->resource('users')->readOnly()->relationships(function ($relations) {
            $relations->hasMany('posts')->except('replace');
        });
    });
});
