<?php

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create()->each(function ($user) {
            $user->posts()->saveMany(factory(Post::class, 30)->make());
        });
    }
}
