<?php
namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model;

class Post extends Model
{
    protected $connection = "mongo";

    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
